﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainForm.aspx.cs" Inherits="WebAppForAlt.MainForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="style.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <div align="center">
            <table>
              <tr>
                  <td><asp:Button class="button" ID="bckBtn" runat="server" Text="Back" OnClick="GoToLastFolder" Visible="false" /></td>
                  <td><asp:Button class="button" ID="crtFoldBtn" runat="server" Text="Create folder" OnClick="OnClickCreateFolderButton" /></td>
                  <td><asp:Button class="button" ID="uplFlBtn" runat="server" Text="Upload file" OnClick="UploadFile" /></td>
                  <td><asp:Button class="button" ID="DltBtn" runat="server" Text="Delete" OnClick="Delete" /></td>
                </tr>
                </table>
            <div id="createFolderArea" visible="false">
                <asp:TextBox ID="folderName" runat="server" Visible="False"></asp:TextBox>
                <asp:Button class="smallButton" ID="okBtn" runat="server" Visible="False" text="OK" OnClick="ConfirmCreatingFolder" Height="28px" Width="33px"/>
            </div>
            <hr />
        </div>
        

    <div align="center">
        <asp:ListView ID="DataList" runat="server" OnItemCommand="OpenFolder">
            <ItemTemplate>
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server"/>
                        </td>
                        <td>
                            <a><asp:LinkButton class="linkButton" align="center" Width="200" ID="path" runat="server" CommandName="sel" CommandArgument='<%#Eval("Path")%>' Text='<%#Eval("Path")%>' /></a>
                        </td>
                        <td>
                            <asp:Label align="center" Width="200" ID="size" runat="server" Text='<%#Eval("Size")%>' />
                        </td>
                        <td>
                            <asp:Label align="center" Width="200" ID="modified" runat="server" Text='<%#Eval("Modified")%>' />
                        </td>
                    </tr>
                </table>

            </ItemTemplate>
                    <LayoutTemplate>
                        <table id="tbl1" runat="server">
                            <tr id="tr1" runat="server" align="center">
                                <td id="td1" runat="server"></td>
                                <td id="td2" runat="server" Width="200">Path</td>
                                <td id="td3" runat="server" Width="200">Size</td>
                                <td id="td4" runat="server" Width="200">Modified</td>
                            </tr>
                            <tr id="ItemPlaceholder" runat="server">  
                            </tr>
                        </table>
                    </LayoutTemplate>
                </asp:ListView>
    </div>
    </form>
</body>
</html>
