﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using Nemiro.OAuth;

namespace WebAppForAlt
{
    public partial class MainForm : System.Web.UI.Page
    {
        private static string CurrentPath = "/";
        public List<Metadata> FileList = new List<Metadata>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetFiles();
            }
        }

        #region Create folder region
        private void CreateFolder()
        {
            var result = OAuthUtility.Post("https://api.dropboxapi.com/1/fileops/create_folder",
                new HttpParameterCollection
                {
                    {"access_token", Session["AccessToken"]},
                    {"root", "auto"},
                    {"path", Path.Combine(CurrentPath, folderName.Text).Replace("\\", "/")}
                }
            );

            if (result.StatusCode == 200)
            {
                GetFiles();
            }
            else
            {
                if (result["error"].HasValue)
                Response.Write(result["error"]);
                else
                Response.Write(result);
            }
        }

        protected void OnClickCreateFolderButton(object sender, EventArgs e)
        {
            if (folderName.Visible == false)
            {
                folderName.Visible = true;
                okBtn.Visible = true;
            }
            else
            {
                folderName.Visible = false;
                okBtn.Visible = false;
            }
        }

        protected void ConfirmCreatingFolder(object sender, EventArgs e)
        {
            if (folderName.Text != "")
            {
                CreateFolder();
                folderName.Text = "";
                folderName.Visible = false;
                okBtn.Visible = false;
            }
            else
            {
                Response.Write("Invalid folder name.");
            }
        }
        #endregion

        #region Navigate region
        protected void OpenFolder(object sender, ListViewCommandEventArgs e)
        {
            CurrentPath = e.CommandArgument.ToString();
            this.GetFiles();
            UpdateBackButton();
        }

        protected void GoToLastFolder(object sender, EventArgs e)
        {
            if (CurrentPath != "/")
            {
               CurrentPath = Path.GetDirectoryName(CurrentPath).Replace("\\", "/");
            }
            GetFiles();
            UpdateBackButton();
        }
        #endregion

        private void GetFiles()
        {
            var result = OAuthUtility.Get("https://api.dropboxapi.com/1/metadata/auto/",
                new HttpParameterCollection
                {
                    {"path", CurrentPath },
                    {"access_token", Session["AccessToken"]}
                }
            );

            if (result.StatusCode == 200)
            {
                foreach (UniValue file in result["contents"])
                {
                    FileList.Add(new Metadata(file["path"].ToString(), file["size"].ToString(), file["modified"].ToString(), Convert.ToBoolean(file["is_dir"])));
                }
                DataList.DataSource = FileList;
                DataList.DataBind();
            }
            else
            {
                if (result["error"].HasValue)
                    Response.Write(result["error"]);
            }
        }

        protected void UploadFile(object sender, EventArgs e)
        {
            Response.Write("Not implemented");
        }

        protected void Delete(object sender, EventArgs e)
        {
            Response.Write("Not implemented");
        }

        #region Visual features
        private void UpdateBackButton()
        {
            if (CurrentPath == "/")
            {
                bckBtn.Visible = false;
            }
            else
            {
                bckBtn.Visible = true;
            }
        }
        #endregion
    }
}