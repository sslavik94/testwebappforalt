﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nemiro.OAuth;

namespace WebAppForAlt
{
    public partial class StartForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            // build callback url
            string returnUrl = new Uri(Request.Url, "LoginResult.aspx").AbsoluteUri;

            // redirect to authorization page
            OAuthWeb.RedirectToAuthorization("Dropbox", returnUrl);
        }
    }
}