﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppForAlt
{
    public class Metadata
    {
        public string Path { get; set; }
        public string Size { get; set; }
        public string Modified { get; set; }
        public bool IsDirectory { get; set; }

        public Metadata(string path, string size, string modified, bool isDirectory)
        {
            Path = path;
            IsDirectory = isDirectory;
            if (IsDirectory != true)
            {
                Size = size;
            }
            else
            {
                Size = "--";
            }
            Modified = modified;
        }
    }
}