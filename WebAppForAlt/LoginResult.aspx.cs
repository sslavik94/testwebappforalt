﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nemiro.OAuth;

namespace WebAppForAlt
{
    public partial class LoginResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string mainFormUrl = new Uri(Request.Url, "MainForm.aspx").AbsoluteUri;
            var result = OAuthWeb.VerifyAuthorization();
            if (result.IsSuccessfully)
            {
                Session["AccessToken"] = result.AccessTokenValue;
                Response.Redirect(mainFormUrl, true); // to home
            }
            else
            {
                Response.Write("Error: " + result.ErrorInfo.Message);
            }
        }
    }
}